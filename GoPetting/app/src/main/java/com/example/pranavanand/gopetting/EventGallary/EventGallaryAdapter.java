package com.example.pranavanand.gopetting.EventGallary;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pranavanand.gopetting.Config;
import com.example.pranavanand.gopetting.Activity.EventActivity;
import com.example.pranavanand.gopetting.Models.Events;
import com.example.pranavanand.gopetting.R;
import com.squareup.picasso.Picasso;

/**
 * Created by pranavanand on 2/8/17.
 */
public class EventGallaryAdapter extends RecyclerView.Adapter<EventGallaryHolder>{


    private Context context;

    public EventGallaryAdapter(Context context) {
        this.context = context;

    }


    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public EventGallaryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_event_layout,parent,false);
        EventGallaryHolder holder = new EventGallaryHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(EventGallaryHolder holder,  int position) {
        Events event = Config.eventList.get(holder.getAdapterPosition());
        setHolder(holder, holder.getAdapterPosition(), event);
    }

    @Override
    public int getItemCount() {
        return Config.eventList.size();
    }


    private void setHolder(final EventGallaryHolder holder, final int position, final Events event){

        // Assign the TextView Required data
        holder.getTv_eventName().setText(event.getName());
        holder.getTv_startTime().setText(event.getStartDate());
        holder.getTv_endTime().setText(event.getEndDate());
        //Set Image
        Picasso.with(context).load(event.getIcon()).into(holder.getIv_logo_image());


        //Add ClickEvents
        holder.getLl_level_4().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When Clicked Add The Event To The CartActivity, Set isAddaedToCart = true , add it to CartList, Remove it from eventList.
                playSound();
                event.setAddedToCart(true);
                Config.cartList.add(event);
                Config.eventList.remove(holder.getAdapterPosition());
                EventActivity.tv_cartCount.setText(Config.cartList.size()+"");
                notifyItemRemoved(holder.getAdapterPosition());

            }
        });
    }


    private void playSound(){
        MediaPlayer mp = MediaPlayer.create(context, R.raw.remove_notify_sound);
        mp.start();
    }


}
