package com.example.pranavanand.gopetting.Models;

import java.util.ArrayList;

/**
 * Created by pranavanand on 2/8/17.
 */
public class Data {
    private String total;
    private ArrayList<Events> data;

    public Data() {
    }

    public Data(String total, ArrayList<Events> data) {
        this.total = total;
        this.data = data;
    }


    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<Events> getData() {
        return data;
    }

    public void setData(ArrayList<Events> data) {
        this.data = data;
    }
}
