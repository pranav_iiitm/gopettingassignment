package com.example.pranavanand.gopetting.EventGallary;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pranavanand.gopetting.R;

/**
 * Created by pranavanand on 2/8/17.
 */
public class EventGallaryHolder extends RecyclerView.ViewHolder{

    private LinearLayout ll_level_1,ll_level_2,ll_level_3,ll_level_4;
    private TextView tv_eventName,tv_startTime,tv_endTime;
    private ImageView iv_logo_image, iv_actionOnCart;

    public EventGallaryHolder(View itemView) {
        super(itemView);

        // Linear Layout
        this.ll_level_1 = (LinearLayout) itemView.findViewById(R.id.ll_level_1);
        this.ll_level_2 = (LinearLayout) itemView.findViewById(R.id.ll_level_2);
        this.ll_level_3 = (LinearLayout) itemView.findViewById(R.id.ll_level_3);
        this.ll_level_4 = (LinearLayout) itemView.findViewById(R.id.ll_level_4);

        // TextView
        this.tv_eventName = (TextView) itemView.findViewById(R.id.tv_eventName);
        this.tv_startTime = (TextView) itemView.findViewById(R.id.tv_startTime);
        this.tv_endTime   = (TextView) itemView.findViewById(R.id.tv_endTime);

        //ImageView
        this.iv_logo_image = (ImageView) itemView.findViewById(R.id.iv_logo_image);
        this.iv_actionOnCart = (ImageView) itemView.findViewById(R.id.iv_actionOnCart);

    }


    public LinearLayout getLl_level_1() {
        return ll_level_1;
    }

    public void setLl_level_1(LinearLayout ll_level_1) {
        this.ll_level_1 = ll_level_1;
    }

    public LinearLayout getLl_level_2() {
        return ll_level_2;
    }

    public void setLl_level_2(LinearLayout ll_level_2) {
        this.ll_level_2 = ll_level_2;
    }

    public LinearLayout getLl_level_3() {
        return ll_level_3;
    }

    public void setLl_level_3(LinearLayout ll_level_3) {
        this.ll_level_3 = ll_level_3;
    }

    public LinearLayout getLl_level_4() {
        return ll_level_4;
    }

    public void setLl_level_4(LinearLayout ll_level_4) {
        this.ll_level_4 = ll_level_4;
    }

    public TextView getTv_eventName() {
        return tv_eventName;
    }

    public void setTv_eventName(TextView tv_eventName) {
        this.tv_eventName = tv_eventName;
    }

    public TextView getTv_startTime() {
        return tv_startTime;
    }

    public void setTv_startTime(TextView tv_startTime) {
        this.tv_startTime = tv_startTime;
    }

    public TextView getTv_endTime() {
        return tv_endTime;
    }

    public void setTv_endTime(TextView tv_endTime) {
        this.tv_endTime = tv_endTime;
    }

    public ImageView getIv_logo_image() {
        return iv_logo_image;
    }

    public void setIv_logo_image(ImageView iv_logo_image) {
        this.iv_logo_image = iv_logo_image;
    }

    public ImageView getIv_actionOnCart() {
        return iv_actionOnCart;
    }

    public void setIv_actionOnCart(ImageView iv_actionOnCart) {
        this.iv_actionOnCart = iv_actionOnCart;
    }
}
