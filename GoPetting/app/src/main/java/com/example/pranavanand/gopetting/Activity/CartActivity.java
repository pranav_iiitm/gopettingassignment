package com.example.pranavanand.gopetting.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pranavanand.gopetting.CartGallay.CartGallaryAdapter;
import com.example.pranavanand.gopetting.Config;
import com.example.pranavanand.gopetting.R;

public class CartActivity extends AppCompatActivity implements View.OnClickListener {


    private final String TAG = "CartActivity";

    RecyclerView recycleView_Cart;
    public static TextView tv_cartCount;
    TextView tv_countCartText;
    ImageView iv_profileIcon;

    CartGallaryAdapter cartGallaryAdapter;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set The content To display.
        findViews();
        setAdapter();
        setRecycleView();
    }


    private void findViews() {
        recycleView_Cart =(RecyclerView) findViewById(R.id.recycleView_Cart);
        tv_countCartText = (TextView) findViewById(R.id.tv_countCartText);
        tv_cartCount = (TextView) findViewById(R.id.tv_cartCount);
        iv_profileIcon = (ImageView) findViewById(R.id.iv_profileIcon);

        tv_cartCount.setText(Config.cartList.size()+"");
        iv_profileIcon.setOnClickListener(this);
    }


    private void setAdapter(){
        cartGallaryAdapter = new CartGallaryAdapter(this);
    }

    private void setRecycleView(){
        layoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recycleView_Cart.setLayoutManager(layoutManager);
        recycleView_Cart.setItemAnimator(new DefaultItemAnimator());
        recycleView_Cart.setAdapter(cartGallaryAdapter);
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.iv_cart || view.getId()==R.id.tv_cartCount){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }

        if(view.getId()==R.id.iv_profileIcon){
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        }
    }

}
