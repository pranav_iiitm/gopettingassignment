package com.example.pranavanand.gopetting;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.pranavanand.gopetting.Models.Events;

import java.util.ArrayList;

/**
 * Created by pranavanand on 2/8/17.
 */


// Keep Track Of all the static and important variable.
public class Config {

    // Base Url for data download.
    public static final String BASEURL = "http://guidebook.com/service/v2/";

    // Output Function
    public static void SOP(String TAG, String message){Log.v(TAG, message);}

    //EventList .. All the events which are not in cartList.
    public static ArrayList<Events> eventList = new ArrayList<Events>();

    //CartList .. All the events added in cart will be present..
    public static ArrayList<Events> cartList = new ArrayList<Events>();

    public static String profileName;
    public static String profilePicUrl;
    public static String email;

    //Check network avibility
    public static boolean isOnline(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }



    public static void recycle_network_checking(final Context c, final CoordinatorLayout coordinatorLayout) {
        try {
            if (!isOnline(c)) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                recycle_network_checking(c, coordinatorLayout);

                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            } else {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Connected", Snackbar.LENGTH_SHORT);

                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.GREEN);
                snackbar.show();

            }
        }
        catch (Exception e)
        {
            SOP("Config",e.getMessage());
        }

    }




}
