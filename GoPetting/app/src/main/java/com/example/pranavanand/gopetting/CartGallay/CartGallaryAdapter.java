package com.example.pranavanand.gopetting.CartGallay;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pranavanand.gopetting.Activity.CartActivity;
import com.example.pranavanand.gopetting.Activity.EventActivity;
import com.example.pranavanand.gopetting.Config;
import com.example.pranavanand.gopetting.EventGallary.EventGallaryHolder;
import com.example.pranavanand.gopetting.Models.Events;
import com.example.pranavanand.gopetting.R;
import com.squareup.picasso.Picasso;

/**
 * Created by pranavanand on 2/8/17.
 */
public class CartGallaryAdapter extends RecyclerView.Adapter<EventGallaryHolder>{


    private Context context;

    public CartGallaryAdapter(Context context) {
        this.context = context;

    }


    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public EventGallaryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_event_layout,parent,false);
        EventGallaryHolder holder = new EventGallaryHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(EventGallaryHolder holder,  int position) {
        Events event = Config.cartList.get(holder.getAdapterPosition());
        setHolder(holder, holder.getAdapterPosition(), event);
    }

    @Override
    public int getItemCount() {
        return Config.cartList.size();
    }


    private void setHolder(final EventGallaryHolder holder, final int position, final Events event){

        // Assign the TextView Required data
        holder.getTv_eventName().setText(event.getName());
        holder.getTv_startTime().setText(event.getStartDate());
        holder.getTv_endTime().setText(event.getEndDate());
        holder.getIv_actionOnCart().setImageResource(R.drawable.ic_remove_shopping_cart_white_36dp);


        //Set Image
        Picasso.with(context).load(event.getIcon()).into(holder.getIv_logo_image());


        //Add ClickEvents
        holder.getLl_level_4().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When Clicked Remove The Event From The CartActivity, Set isAddaedToCart = false , add it to EventList, Remove it from CartList.
                playSound();
                event.setAddedToCart(false);
                Config.eventList.add(event);
                Config.cartList.remove(holder.getAdapterPosition());
                EventActivity.tv_cartCount.setText(Config.cartList.size()+"");
                CartActivity.tv_cartCount.setText(Config.cartList.size()+"");
                notifyItemRemoved(holder.getAdapterPosition());

            }
        });
    }


    private void playSound(){
        MediaPlayer mp = MediaPlayer.create(context, R.raw.remove_notify_sound);
        mp.start();
    }


}
