package com.example.pranavanand.gopetting.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pranavanand.gopetting.Config;
import com.example.pranavanand.gopetting.EventGallary.EventGallaryAdapter;
import com.example.pranavanand.gopetting.Interfaces.Download;
import com.example.pranavanand.gopetting.Models.Data;
import com.example.pranavanand.gopetting.R;


import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


public class EventActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = "EventActivity";

    RecyclerView recycleView_Events;
    CoordinatorLayout cl_mainActivity;
    ImageView iv_cart;
    public static TextView tv_cartCount;
    ImageView iv_profileIcon;


    EventGallaryAdapter eventGallaryAdapter;
    LinearLayoutManager layoutManager;
    public ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Actions
        resetStaticVarialbles();
        findViews();
        recycle_network_checking(cl_mainActivity);


    }

    private void resetStaticVarialbles() {
        Config.cartList.clear();
        Config.eventList.clear();

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        Config.email =sharedPref.getString("email", "noEmail");
        Config.profileName = sharedPref.getString("personName", "noPersonName");
        Config.profilePicUrl = sharedPref.getString("personPhotoUrl", "noPersonPhotoUrl");

    }


    @Override
    protected void onResume() {
        super.onResume();

        if(eventGallaryAdapter!=null)
            eventGallaryAdapter.notifyDataSetChanged();
    }



    private void findViews() {
        recycleView_Events =(RecyclerView) findViewById(R.id.recycleView_Events);
        cl_mainActivity = (CoordinatorLayout) findViewById(R.id.cl_mainActivity);
        iv_cart = (ImageView) findViewById(R.id.iv_cart);
        tv_cartCount = (TextView) findViewById(R.id.tv_cartCount);
        iv_profileIcon = (ImageView) findViewById(R.id.iv_profileIcon);

        setDataToWidgets();
        addClickOnWidgets();
    }

    private void setDataToWidgets(){
        tv_cartCount.setText(Config.cartList.size()+"");
    }

    private void addClickOnWidgets(){
        iv_cart.setOnClickListener(this);
        tv_cartCount.setOnClickListener(this);
        iv_profileIcon.setOnClickListener(this);
    }


    private void setAdapter(){
        eventGallaryAdapter = new EventGallaryAdapter(this);
    }

    private void setRecycleView(){
        layoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recycleView_Events.setLayoutManager(layoutManager);
        recycleView_Events.setItemAnimator(new DefaultItemAnimator());
        recycleView_Events.setAdapter(eventGallaryAdapter);
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.iv_cart || view.getId()==R.id.tv_cartCount){
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        }

        if(view.getId()==R.id.iv_profileIcon){
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        }
    }




    private void downloadData(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Config.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Download api = retrofit.create(Download.class);
        Call<Data> response = api.getData();
        response.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Response<Data> response, Retrofit retrofit) {
                Config.SOP(TAG, "onResponse");
                Config.eventList = response.body().getData();
                // Hide the progress bar
                progressBar.hide();
                // Set The content To display.
                setAdapter();
                setRecycleView();
            }

            @Override
            public void onFailure(Throwable t) {
                // Error occured in downloading...
                Config.SOP(TAG, t.getMessage());
            }


        });

    }


    //start progressBar indication download of data.
    private void setProgressDialog(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Connecting...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();
    }


    private void recycle_network_checking(final CoordinatorLayout coordinatorLayout) {
        try {
            if (!Config.isOnline(this)) {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE)
                                            .setAction("RETRY", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                recycle_network_checking(coordinatorLayout);

                                            }
                                        });

                snackbar.setActionTextColor(Color.RED);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            } else {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Connected", Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.GREEN);
                snackbar.show();
                setProgressDialog();
                downloadData();

            }
        }
        catch (Exception e)
        {
            Config.SOP("Config",e.getMessage());
        }

    }


}
