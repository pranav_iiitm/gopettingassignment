package com.example.pranavanand.gopetting.Models;

/**
 * Created by pranavanand on 2/8/17.
 */
public class Events {

    private String  startDate;
    private String  objType;
    private String  endDate;
    private String  name;
    private boolean loginRequired;
    private String  url;
    private Venue   venue;
    private String  icon;
    private boolean isAddedToCart;


    public Events() {
    }

    public Events(String startDate, String objType, String endDate, String name, boolean loginRequired, String url, Venue venue, String icon, boolean isAddedToCart) {
        this.startDate = startDate;
        this.objType = objType;
        this.endDate = endDate;
        this.name = name;
        this.loginRequired = loginRequired;
        this.url = url;
        this.venue = venue;
        this.icon = icon;
        this.isAddedToCart = isAddedToCart;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLoginRequired() {
        return loginRequired;
    }

    public void setLoginRequired(boolean loginRequired) {
        this.loginRequired = loginRequired;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setAddedToCart(boolean addedToCart) {
        isAddedToCart = addedToCart;
    }

    public boolean isAddedToCart() {
        return isAddedToCart;
    }
}