package com.example.pranavanand.gopetting.Interfaces;

import com.example.pranavanand.gopetting.Models.Data;


import java.util.List;

import retrofit.Call;
import retrofit.http.GET;


/**
 * Created by pranavanand on 2/8/17.
 */
public interface Download {
    @GET("upcomingGuides/")
    Call<Data> getData();
}
